#![deny(
//    missing_docs,
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications
)]

use crate::error::WikibaseError;
use crate::Value;
use serde::ser::{SerializeStruct, Serializer};
use serde::Serialize;

/// DataValueType
///
/// Is the type that is stored together with a value in the DataValue.
///
/// # JSON Mapping
///
/// EntityId = "wikibase-entityid"
/// GlobeCoordinate = "globecoordinate"
/// MonoLingualText = "monolingualtext"
/// Quantity = "quantity"
/// StringType = "string"
/// Time = "time"
///
/// # Example
///
/// ```
/// let data_value_type = wikibase::DataValueType::new_from_str("quantity");
/// ```
#[derive(Debug, Clone, PartialEq, Copy)]
pub enum DataValueType {
    EntityId,
    GlobeCoordinate,
    MonoLingualText,
    EntitySchemaId,
    Quantity,
    StringType,
    Time,
}

impl DataValueType {
    pub fn new_from_str(string_value: &str) -> Result<DataValueType, WikibaseError> {
        match string_value {
            "globecoordinate" => Ok(DataValueType::GlobeCoordinate),
            "monolingualtext" => Ok(DataValueType::MonoLingualText),
            "quantity" => Ok(DataValueType::Quantity),
            "string" => Ok(DataValueType::StringType),
            "wikibase-entityid" => Ok(DataValueType::EntityId),
            "entity-schema" => Ok(DataValueType::EntitySchemaId),
            "time" => Ok(DataValueType::Time),
            _ => Err(WikibaseError::Serialization(
                "Data value type could not be matched".to_string(),
            )),
        }
    }

    pub fn string_value(&self) -> String {
        match *self {
            DataValueType::EntityId => "wikibase-entityid".to_string(),
            DataValueType::GlobeCoordinate => "globecoordinate".to_string(),
            DataValueType::MonoLingualText => "monolingualtext".to_string(),
            DataValueType::EntitySchemaId => "entity-schema".to_string(),
            DataValueType::Quantity => "quantity".to_string(),
            DataValueType::StringType => "string".to_string(),
            DataValueType::Time => "time".to_string(),
        }
    }
}

impl Serialize for DataValueType {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let str_ptr = self.string_value();
        serializer.serialize_str(&str_ptr)
    }
}

impl DataValue {
    pub fn new(value_type: DataValueType, value: Value) -> DataValue {
        Self { value_type, value }
    }

    pub fn set_value(&mut self, value: Value) {
        self.value = value;
    }

    pub fn set_value_type(&mut self, value_type: DataValueType) {
        self.value_type = value_type;
    }

    pub fn value(&self) -> &Value {
        &self.value
    }

    pub fn value_type(&self) -> &DataValueType {
        &self.value_type
    }
}

impl Serialize for DataValue {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut state = serializer.serialize_struct("DataValue", 2)?;
        state.serialize_field("type", &self.value_type)?;
        state.serialize_field("value", &self.value)?;
        state.end()
    }
}

/// DataValue
///
/// The DataValue holds a value_type (wikibase-item, ...) and the
/// actual value. It is stored within a Snak struct.
///
/// # Json mapping
///
/// "value" - value
/// "type" - value_type
#[derive(Debug, Clone, PartialEq)]
pub struct DataValue {
    value: Value,
    value_type: DataValueType,
}
