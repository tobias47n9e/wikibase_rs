#![deny(
//    missing_docs,
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications
)]

use crate::deserialize::{FromJson, ToJson};
use crate::entity::EntityTrait;
use crate::entity_type::EntityType;
use crate::error::WikibaseError;
use crate::sitelink::*;
use crate::statement::*;
use crate::LocaleString;

#[derive(Debug, Clone)]
pub struct ItemEntity {
    id: String,
    labels: Vec<LocaleString>,
    descriptions: Vec<LocaleString>,
    aliases: Vec<LocaleString>,
    claims: Vec<Statement>,
    sitelinks: Option<Vec<SiteLink>>,
    missing: bool,
    entity_type: EntityType,
}

impl ToJson for ItemEntity {}
impl FromJson for ItemEntity {}

impl EntityTrait for ItemEntity {
    fn id_mut(&mut self) -> &mut String {
        &mut self.id
    }

    fn missing_mut(&mut self) -> &mut bool {
        &mut self.missing
    }

    fn aliases_mut(&mut self) -> &mut Vec<LocaleString> {
        &mut self.aliases
    }

    fn labels_mut(&mut self) -> &mut Vec<LocaleString> {
        &mut self.labels
    }

    fn descriptions_mut(&mut self) -> &mut Vec<LocaleString> {
        &mut self.descriptions
    }

    fn claims_mut(&mut self) -> &mut Vec<Statement> {
        &mut self.claims
    }

    fn sitelinks_mut(&mut self) -> &mut Option<Vec<SiteLink>> {
        &mut self.sitelinks
    }

    fn entity_type_mut(&mut self) -> &mut EntityType {
        &mut self.entity_type
    }

    fn id(&self) -> &String {
        &self.id
    }

    fn missing(&self) -> bool {
        self.missing
    }

    fn aliases(&self) -> &Vec<LocaleString> {
        &self.aliases
    }

    fn labels(&self) -> &Vec<LocaleString> {
        &self.labels
    }

    fn descriptions(&self) -> &Vec<LocaleString> {
        &self.descriptions
    }

    fn claims(&self) -> &Vec<Statement> {
        &self.claims
    }

    fn sitelinks(&self) -> &Option<Vec<SiteLink>> {
        &self.sitelinks
    }

    fn entity_type(&self) -> &EntityType {
        &self.entity_type
    }

    fn to_json(&self) -> serde_json::Value {
        let mut ret = json!({
            "labels":&self.locale_strings_to_json(self.labels()),
            "descriptions":&self.locale_strings_to_json(self.descriptions()),
            "aliases":&self.aliases_to_json(self.aliases()),
            "sitelinks":&self.sitelinks_to_json(self.sitelinks()),
            "claims":&self.statements_to_json(self.claims()),
            "type":EntityType::new_from_id(&self.id()).ok(),
        });

        // ID
        if !self.id().is_empty() {
            ret["id"] = json!(self.id());
        }

        ret
    }
}

impl ItemEntity {
    pub fn new(
        id: String,
        labels: Vec<LocaleString>,
        descriptions: Vec<LocaleString>,
        aliases: Vec<LocaleString>,
        claims: Vec<Statement>,
        sitelinks: Option<Vec<SiteLink>>,
        missing: bool,
    ) -> Self {
        Self {
            id,
            labels,
            descriptions,
            aliases,
            claims,
            sitelinks,
            missing,
            entity_type: EntityType::Item,
        }
    }

    pub fn new_empty() -> Self {
        Self {
            id: "".to_string(),
            labels: vec![],
            descriptions: vec![],
            aliases: vec![],
            claims: vec![],
            sitelinks: None,
            missing: false,
            entity_type: EntityType::Item,
        }
    }

    pub fn new_missing() -> Self {
        let mut ret = Self::new_empty();
        *ret.missing_mut() = true;
        ret
    }

    pub fn new_from_json(json: &serde_json::Value) -> Result<Self, WikibaseError> {
        match json.get("missing") {
            Some(_) => Ok(Self::new_missing()),
            _ => Ok(Self::new(
                json["id"]
                    .as_str()
                    .ok_or_else(|| WikibaseError::Serialization("ID missing".to_string()))
                    .map(|s| s.to_string())?,
                Self::locale_strings_from_json(&json, "labels")?,
                Self::locale_strings_from_json(&json, "descriptions")?,
                Self::locale_string_array_from_json(&json, "aliases")?,
                Self::statements_from_json(&json)?,
                Self::sitelinks_from_json(&json)?,
                false,
            )),
        }
    }
}
