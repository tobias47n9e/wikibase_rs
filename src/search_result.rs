#![deny(
//    missing_docs,
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications
)]

use crate::configuration::Configuration;
use crate::entity_type::EntityType;
use crate::error::WikibaseError;
use crate::from_json;
use crate::query::SearchQuery;
use crate::LocaleString;

/// Search-result entity
///
/// Struct that holds all the data about a search result. The difference
/// to a normal entity is that no claims and sitelinks are returned. The
/// label, description and aliases are only returned for one language.
#[derive(Debug, Clone)]
pub struct SearchResultEntity {
    id: String,
    _entity_type: EntityType,
    label: LocaleString,
    description: Option<LocaleString>,
    aliases: Vec<LocaleString>,
}

impl SearchResultEntity {
    pub fn new<S: Into<String>>(
        id: S,
        _entity_type: EntityType,
        label: LocaleString,
        description: Option<LocaleString>,
        aliases: Vec<LocaleString>,
    ) -> SearchResultEntity {
        Self {
            id: id.into(),
            _entity_type,
            label,
            description,
            aliases,
        }
    }

    pub fn aliases(&self) -> &Vec<LocaleString> {
        &self.aliases
    }

    pub fn description(&self) -> &Option<LocaleString> {
        &self.description
    }

    pub fn id(&self) -> &str {
        &self.id
    }

    pub fn label(&self) -> &LocaleString {
        &self.label
    }

    pub fn set_aliases(&mut self, aliases: Vec<LocaleString>) {
        self.aliases = aliases;
    }

    pub fn set_descriptions(&mut self, description: Option<LocaleString>) {
        self.description = description;
    }

    pub fn set_labels(&mut self, label: LocaleString) {
        self.label = label;
    }
}

/// Search results
///
/// A struct holding a vector of search result entities.
#[derive(Debug, Clone)]
pub struct SearchResults {
    results: Vec<SearchResultEntity>,
}

impl SearchResults {
    pub fn new(results: Vec<SearchResultEntity>) -> SearchResults {
        Self { results }
    }

    /// Takes a wikibase entity query and returns a result of an error.
    pub async fn new_from_query(
        query: &SearchQuery,
        configuration: &Configuration,
    ) -> Result<SearchResults, WikibaseError> {
        let params = query.params();
        let request_result = configuration.api().get_query_api_json_all(&params).await;

        let json_response = match request_result {
            Ok(value) => value,
            Err(_error) => return Err(WikibaseError::Configuration("Search failed".to_string())),
        };

        from_json::search_result_entities_from_json(&json_response, &query)
    }

    pub fn results(&self) -> &Vec<SearchResultEntity> {
        &self.results
    }
}
