#![deny(
//    missing_docs,
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications
)]

use crate::error::WikibaseError;
use serde::{Deserialize, Serialize};

/// LocaleString
///
/// Structure holding a language key and a value string. Used for
/// labels and descriptions of Wikidata items.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct LocaleString {
    language: String,
    value: String,
}

impl PartialEq for LocaleString {
    fn eq(&self, other: &LocaleString) -> bool {
        self.language == other.language && self.value == other.value
    }
}
impl Eq for LocaleString {}

impl LocaleString {
    pub fn new<S: Into<String>>(language: S, value: S) -> LocaleString {
        Self {
            language: language.into(),
            value: value.into(),
        }
    }

    /// Serializes a locale string
    ///
    /// A locale string is a JSON object:
    /// {"language": "de", "value": "Label"}
    ///
    /// Returns a LocaleString inside of an option:
    /// LocaleString {language: "de", value: "Label"}
    pub fn new_from_json(string_object: &serde_json::Value) -> Result<LocaleString, WikibaseError> {
        let language = match string_object["language"].as_str() {
            Some(value) => value,
            None => {
                return Err(WikibaseError::Serialization(
                    "Can't deserialize language in locale string".to_string(),
                ));
            }
        };

        match string_object["value"].as_str() {
            Some(value) => Ok(Self::new(language, value)),
            None => Err(WikibaseError::Serialization(
                "Can't deserialize value in locale string".to_string(),
            )),
        }
    }

    pub fn language(&self) -> &str {
        &self.language
    }

    pub fn value(&self) -> &str {
        &self.value
    }

    pub fn set_value(&mut self, value: &str) {
        self.value = value.into();
    }
}
