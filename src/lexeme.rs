#![deny(
//    missing_docs,
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications
)]

use crate::deserialize::{FromJson, ToJson};
use crate::entity::EntityTrait;
use crate::entity_type::EntityType;
use crate::error::WikibaseError;
use crate::sitelink::*;
use crate::statement::*;
use crate::EntityValue;
use crate::LocaleString;

#[derive(Debug, Clone)]
pub struct LexemeForm {
    id: String,
    representations: Vec<LocaleString>,
    grammatical_features: Vec<EntityValue>,
    claims: Vec<Statement>,
}

impl ToJson for LexemeForm {}
impl FromJson for LexemeForm {}

impl LexemeForm {
    pub fn new(
        id: String,
        representations: Vec<LocaleString>,
        grammatical_features: Vec<EntityValue>,
        claims: Vec<Statement>,
    ) -> Self {
        Self {
            id,
            representations,
            grammatical_features,
            claims,
        }
    }

    pub fn new_empty() -> Self {
        Self {
            id: "".to_string(),
            representations: vec![],
            grammatical_features: vec![],
            claims: vec![],
        }
    }

    pub fn new_from_json(json: &serde_json::Value) -> Result<Self, WikibaseError> {
        let mut ret = Self::new_empty();
        if let Some(id) = json["id"].as_str() {
            ret.id = id.to_string()
        }
        if let Some(array) = json["grammaticalFeatures"].as_array() {
            for id in array {
                match id.as_str() {
                    Some(id) => {
                        let id = id.to_string();
                        let entity_type = EntityType::new_from_id(&id)?;
                        let ev = EntityValue::new(entity_type, id);
                        ret.grammatical_features.push(ev);
                    }
                    None => {}
                }
            }
        }
        ret.representations = Self::locale_strings_from_json(&json, "representations")?;
        ret.claims = Self::statements_from_json(&json)?;
        Ok(ret)
    }

    pub fn to_json(&self) -> serde_json::Value {
        let ret = json!({
            "id": self.id,
            "representations":&self.locale_strings_to_json(self.representations()),
            "claims":&self.statements_to_json(self.claims()),
            "grammaticalFeatures":self.grammatical_features.iter().map(|gf|gf.id()).collect::<Vec<&str>>()
        });
        ret
    }

    pub fn id(&self) -> &String {
        &self.id
    }

    pub fn id_mut(&mut self) -> &mut String {
        &mut self.id
    }

    pub fn representations(&self) -> &Vec<LocaleString> {
        &self.representations
    }

    pub fn representations_mut(&mut self) -> &mut Vec<LocaleString> {
        &mut self.representations
    }

    pub fn grammatical_features(&self) -> &Vec<EntityValue> {
        &self.grammatical_features
    }

    pub fn grammatical_features_mut(&mut self) -> &mut Vec<EntityValue> {
        &mut self.grammatical_features
    }

    pub fn claims(&self) -> &Vec<Statement> {
        &self.claims
    }

    pub fn claims_mut(&mut self) -> &mut Vec<Statement> {
        &mut self.claims
    }
}

#[derive(Debug, Clone)]
pub struct LexemeSense {
    id: String,
    glosses: Vec<LocaleString>,
    claims: Vec<Statement>,
}

impl ToJson for LexemeSense {}
impl FromJson for LexemeSense {}

impl LexemeSense {
    pub fn new(id: String, glosses: Vec<LocaleString>, claims: Vec<Statement>) -> Self {
        Self {
            id,
            glosses,
            claims,
        }
    }

    pub fn new_empty() -> Self {
        Self {
            id: "".to_string(),
            glosses: vec![],
            claims: vec![],
        }
    }

    pub fn new_from_json(json: &serde_json::Value) -> Result<Self, WikibaseError> {
        let mut ret = Self::new_empty();
        if let Some(id) = json["id"].as_str() {
            ret.id = id.to_string()
        }
        ret.glosses = Self::locale_strings_from_json(&json, "glosses")?;
        ret.claims = Self::statements_from_json(&json)?;
        Ok(ret)
    }

    pub fn to_json(&self) -> serde_json::Value {
        let ret = json!({
            "id": self.id,
            "glosses":&self.locale_strings_to_json(self.glosses()),
            "claims":&self.statements_to_json(self.claims()),
        });
        ret
    }

    pub fn id(&self) -> &String {
        &self.id
    }

    pub fn id_mut(&mut self) -> &mut String {
        &mut self.id
    }

    pub fn glosses(&self) -> &Vec<LocaleString> {
        &self.glosses
    }

    pub fn glosses_mut(&mut self) -> &mut Vec<LocaleString> {
        &mut self.glosses
    }

    pub fn claims(&self) -> &Vec<Statement> {
        &self.claims
    }

    pub fn claims_mut(&mut self) -> &mut Vec<Statement> {
        &mut self.claims
    }
}

#[derive(Debug, Clone)]
pub struct LexemeEntity {
    id: String,
    lemmas: Vec<LocaleString>,
    descriptions: Vec<LocaleString>,
    aliases: Vec<LocaleString>,
    claims: Vec<Statement>,
    sitelinks: Option<Vec<SiteLink>>,
    missing: bool,
    entity_type: EntityType,

    // Special for Lexeme
    lexical_category: Option<String>,
    language: String,
    forms: Vec<LexemeForm>,
    senses: Vec<LexemeSense>,
}

impl ToJson for LexemeEntity {}
impl FromJson for LexemeEntity {}

impl EntityTrait for LexemeEntity {
    fn id_mut(&mut self) -> &mut String {
        &mut self.id
    }

    fn missing_mut(&mut self) -> &mut bool {
        &mut self.missing
    }

    fn aliases_mut(&mut self) -> &mut Vec<LocaleString> {
        &mut self.aliases
    }

    fn labels_mut(&mut self) -> &mut Vec<LocaleString> {
        &mut self.lemmas
    }

    fn descriptions_mut(&mut self) -> &mut Vec<LocaleString> {
        &mut self.descriptions
    }

    fn claims_mut(&mut self) -> &mut Vec<Statement> {
        &mut self.claims
    }

    fn sitelinks_mut(&mut self) -> &mut Option<Vec<SiteLink>> {
        &mut self.sitelinks
    }

    fn entity_type_mut(&mut self) -> &mut EntityType {
        &mut self.entity_type
    }

    fn id(&self) -> &String {
        &self.id
    }

    fn missing(&self) -> bool {
        self.missing
    }

    fn aliases(&self) -> &Vec<LocaleString> {
        &self.aliases
    }

    fn labels(&self) -> &Vec<LocaleString> {
        &self.lemmas
    }

    fn descriptions(&self) -> &Vec<LocaleString> {
        &self.descriptions
    }

    fn claims(&self) -> &Vec<Statement> {
        &self.claims
    }

    fn sitelinks(&self) -> &Option<Vec<SiteLink>> {
        &self.sitelinks
    }

    fn entity_type(&self) -> &EntityType {
        &self.entity_type
    }

    fn to_json(&self) -> serde_json::Value {
        // TODO specialize
        let mut ret = json!({
            "lemmas":&self.locale_strings_to_json(self.labels()),
            "claims":&self.statements_to_json(self.claims()),
            "type":EntityType::new_from_id(&self.id()).ok(),
            "lexicalCategory":self.lexical_category(),
            "language":self.language(),
            "forms":json!(self.forms.iter().map(|f|f.to_json()).collect::<Vec<serde_json::Value>>()),
            "senses":json!(self.senses.iter().map(|s|s.to_json()).collect::<Vec<serde_json::Value>>()),
        });

        // ID
        if !self.id().is_empty() {
            ret["id"] = json!(self.id());
        }

        ret
    }
}

impl LexemeEntity {
    pub fn new(
        id: String,
        lemmas: Vec<LocaleString>,
        claims: Vec<Statement>,
        language: String,
        lexical_category: Option<String>,
        forms: Vec<LexemeForm>,
        senses: Vec<LexemeSense>,
        missing: bool,
    ) -> Self {
        Self {
            id,
            lemmas,
            descriptions: vec![],
            aliases: vec![],
            claims,
            sitelinks: None,
            missing,
            entity_type: EntityType::Lexeme,
            lexical_category,
            language,
            forms,
            senses,
        }
    }

    pub fn new_empty() -> Self {
        Self {
            id: "".to_string(),
            lemmas: vec![],
            descriptions: vec![],
            aliases: vec![],
            claims: vec![],
            sitelinks: None,
            missing: false,
            entity_type: EntityType::Lexeme,
            lexical_category: None,
            language: "".to_string(),
            forms: vec![],
            senses: vec![],
        }
    }

    pub fn new_missing() -> Self {
        let mut ret = Self::new_empty();
        *ret.missing_mut() = true;
        ret
    }

    pub fn new_from_json(json: &serde_json::Value) -> Result<Self, WikibaseError> {
        match json.get("missing") {
            Some(_) => Ok(Self::new_missing()),
            _ => Ok(Self::new(
                json["id"]
                    .as_str()
                    .ok_or_else(|| WikibaseError::Serialization("ID missing".to_string()))
                    .map(|s| s.to_string())?,
                Self::locale_strings_from_json(&json, "lemmas")?,
                Self::statements_from_json(&json)?,
                json["language"]
                    .as_str()
                    .ok_or_else(|| WikibaseError::Serialization("language missing".to_string()))
                    .map(|s| s.to_string())?,
                json["lexicalCategory"].as_str().map(|s| s.to_string()),
                Self::forms_from_json(&json)?,
                Self::senses_from_json(&json)?,
                false,
            )),
        }
    }

    pub fn lexical_category(&self) -> &Option<String> {
        &self.lexical_category
    }

    pub fn lexical_category_mut(&mut self) -> &mut Option<String> {
        &mut self.lexical_category
    }

    pub fn language(&self) -> &String {
        &self.language
    }

    pub fn language_mut(&mut self) -> &mut String {
        &mut self.language
    }

    pub fn forms(&self) -> &Vec<LexemeForm> {
        &self.forms
    }

    pub fn forms_mut(&mut self) -> &mut Vec<LexemeForm> {
        &mut self.forms
    }

    pub fn senses(&self) -> &Vec<LexemeSense> {
        &self.senses
    }

    pub fn senses_mut(&mut self) -> &mut Vec<LexemeSense> {
        &mut self.senses
    }
}

#[cfg(test)]
mod tests {
    use crate::entity_container::*;
    use mediawiki::api::Api;

    #[tokio::test]
    async fn test_lexeme1() {
        let api = Api::new("https://www.wikidata.org/w/api.php")
            .await
            .unwrap();
        let ec = EntityContainer::new();
        let lexeme = ec.load_entity(&api, "L2").await.unwrap();
        dbg!(lexeme);
    }
}
