// lib.rs
//
// Copyright © 2018
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#![deny(
//    missing_docs,
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications
)]

extern crate curl;
pub extern crate mediawiki;
extern crate rand;
extern crate rayon;
extern crate regex;
extern crate serde;
#[macro_use]
extern crate serde_json;

pub mod configuration;
pub mod datavalue;
pub mod deserialize;
pub mod entity;
pub mod entity_container;
pub mod entity_diff;
pub mod entity_type;
pub mod error;
pub mod from_json;
pub mod item;
pub mod lexeme;
pub mod locale_string;
pub mod mediainfo;
pub mod property;
pub mod query;
pub mod reference;
pub mod requests;
pub mod search_result;
pub mod sitelink;
pub mod snak;
pub mod statement;
pub mod validate;
pub mod value;

pub use crate::item::*;
pub use configuration::*;
pub use datavalue::*;
pub use entity::*;
pub use entity_type::*;
pub use error::*;
pub use lexeme::*;
pub use locale_string::*;
pub use mediainfo::*;
pub use property::*;
pub use query::SearchQuery;
pub use reference::*;
pub use search_result::*;
pub use sitelink::*;
pub use snak::*;
pub use statement::*;
pub use value::*;
