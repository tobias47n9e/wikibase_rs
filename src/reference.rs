#![deny(
//    missing_docs,
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications
)]

use crate::snak::Snak;
use serde::Serialize;

/// Reference
///
/// A Reference holds a vector of Snaks.
#[derive(Debug, Clone, Serialize, PartialEq)]
pub struct Reference {
    snaks: Vec<Snak>,
}

impl Reference {
    pub fn new(snaks: Vec<Snak>) -> Reference {
        Self { snaks }
    }

    pub fn set_snaks(&mut self, snaks: Vec<Snak>) {
        self.snaks = snaks;
    }

    pub fn snaks(&self) -> &Vec<Snak> {
        &self.snaks
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn reference() {
        let snak = vec![Snak::new_string("P123", "foo")];
        let mut reference = Reference::new(snak.clone());
        assert_eq!(*reference.snaks(), snak);
        reference.set_snaks(vec![]);
        assert_eq!(*reference.snaks(), vec![]);
    }
}
