extern crate wikibase;

use wikibase::query;

/// Test if the URL is changed from the default (Wikidata) to another instance
///
/// Setting the environment variable should change the request URL that
/// is returned by a query.
#[tokio::test]
async fn test_api_url() {
    let mut configuration = wikibase::Configuration::new("Testing/0.1").await.unwrap();
    let entity_query = query::EntityQuery::new(vec!["Q43453".to_string()], "uk");
    assert_eq!("https://www.wikidata.org/w/api.php?action=wbgetentities&ids=Q43453&languages=uk&uselang=uk&format=json",
    entity_query.url(&configuration));

    configuration
        .set_api_url("https://test.wikidata.org/w/api.php")
        .await;
    assert_eq!("https://test.wikidata.org/w/api.php?action=wbgetentities&ids=Q43453&languages=uk&uselang=uk&format=json",
    entity_query.url(&configuration));
}

/// Test if user-agent is constructed correctly
///
/// User-agent has to consist of the user user-agent prefix, and the
/// Wikibase user-agent.
#[tokio::test]
async fn test_user_agent() {
    let start = "Testing/0.1";
    let expected = format!("{} Wikibase-RS/{}", &start, env!("CARGO_PKG_VERSION"));
    let configuration = wikibase::Configuration::new(start).await.unwrap();
    assert_eq!(expected, configuration.user_agent());
}
